import React from 'react';
import './DiagramSetter.scss';
import FilterButton from '../../dumbComponents/FilterButton/FilterButton';
class DiagramSetter extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange(e) {
        this.props.onChangeRadio(e.target.value);
    }
    render() {
        return (
            <div className={`diagram-interface ${this.props.darkMode ? 'theme-dark' : ''}`}>
                <h3 className="diagram-interface__article">Filter by:</h3>
                <form className="diagram-interface__form" onChange={this.onChange}>
                    <FilterButton darkMode={this.props.darkMode} name={'percent'} isChecked={true} value={'percent'}/>
                    <FilterButton darkMode={this.props.darkMode} name={'percent 1 hour'} value={'percent_change_1h'}/>
                    <FilterButton darkMode={this.props.darkMode} name={'percent 7 days'} value={'percent_change_7d'}/>
                    <FilterButton darkMode={this.props.darkMode} name={'percent 24 hours'} value={'percent_change_24h'}/>
                </form>
            </div>
        )
    }
}

export default DiagramSetter;
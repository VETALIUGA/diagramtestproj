import React from 'react';
import './Header.scss';

const Header = (props) => {
    return (
        <div className={`header ${props.darkMode ? 'theme-dark' : ''}`}>
            <header className="section header__section">
                <div className="container header__container">
                    <div className="header__grid">
                        <div className="header__grid-item">
                            <h1 className="header__article">Cryptocurrency</h1>
                        </div>
                        <div className="header__grid-item">
                            <span className="header__button-desc">Theme switcher</span>
                            <button onClick={props.changeMode} className="header__button"></button>

                        </div>
                    </div>
                </div>
            </header>
        </div>
    )
};

export default Header;
import React from 'react';
import './DiagramItem.scss';

const DiagramItem = (props) => {
    const isPositive = Math.sign(props.percent) === 1;
    const styleValue = isPositive ?
        { top: (100 - (props.percent / (props.currentHeight) * 100)) / 2 + '%', bottom: '50%', transition: 'bottom 0s, top 0.5s' }
        :
        { top: '50%', bottom: (100 - (Math.abs(props.percent) / props.currentHeight * 100)) / 2 + '%', transition: 'bottom 0.5s, top 0s' };

    return (
        <div className={`diagram-item ${props.darkMode ? 'theme-dark' : ''}`}>
            <div
                className={`diagram-item__value ${isPositive ? '' : 'diagram-item__value--negative'}`}
                style={{ ...styleValue }}
                data-counter={props.symbol}
                data-value={props.percent}>
            </div>
            <div className="diagram-item__name">{props.name}</div>
        </div>
    )
};

export default DiagramItem;
import React from 'react';
import './FilterButton.scss';

const FilterButton = (props) => {  
    return (
        <div className={`filter-button ${props.darkMode ? 'theme-dark' : ''}`}>
            <label className="filter-button__label">
            <span className="filter-button__name">{props.name}</span>
                <input
                    className="filter-button__radio"
                    defaultChecked={props.isChecked || false}
                    name={props.groupName || 'defaultGroup'}
                    type="radio"
                    value={props.value}
                />
            </label>
        </div>
    )
};

export default FilterButton;
import React from 'react';
import './App.scss';
import Diagram from './smartComponents/Diagram/Diagram';
import Header from './dumbComponents/Header/Header';

class App extends React.Component {
  state = {
    darkMode: false
  }

  changeCurrentMode() {
    const currentMode = this.state.darkMode;
    this.setState({
      ...this.state,
      darkMode: !currentMode
    })
    localStorage.setItem('dark', JSON.stringify(!currentMode));
  }

  getCurrentMode() {
    const savedMode = JSON.parse(localStorage.getItem('dark'));
    if(savedMode === null) {
      localStorage.setItem('dark', JSON.stringify('false'))
    } else {
      this.setState({ ...this.state, darkMode: savedMode })
    }
  }

  componentDidMount() {
    this.getCurrentMode();

  }

  render() {
    return (
      <div className="app">
        <Header changeMode={() => this.changeCurrentMode()} darkMode={this.state.darkMode} />
        <Diagram darkMode={this.state.darkMode} />
      </div>
    )
  };
}

export default App;
